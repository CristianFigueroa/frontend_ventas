import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListarProductoComponent } from './components/producto/listar/listar-producto.component';
import { DetalleProductoComponent } from './components/producto/detalle/detalle-producto.component';
import { NuevoProductoComponent } from './components/producto/nuevo/nuevo-producto.component';
import { EditarProductoComponent } from './components/producto/editar/editar-producto.component';
import { LoginUsuarioComponent } from './components/usuario/login/login-usuario.component';
import { RegistroComponent } from './components/usuario/registro/registro.component';
import { IndexComponent } from './components/index/index/index.component';
import { GuardService as guard } from './services/guard.service';
import { BorrarComponent } from './components/producto/borrar/borrar.component';


const routes: Routes = [
  { path: '', component: ListarProductoComponent },
  { path: 'login', component: LoginUsuarioComponent },
  { path: 'index', component: IndexComponent },
  { path: 'registro', component: RegistroComponent },
  { path: 'borrar/:id', component: BorrarComponent, canActivate: [guard], data: { expectedRol: ['admin'] } },
  { path: 'detalle/:id', component: DetalleProductoComponent },
  { path: 'nuevo', component: NuevoProductoComponent, canActivate: [guard], data: { expectedRol: ['admin'] } },
  { path: 'editar/:id', component: EditarProductoComponent, canActivate: [guard], data: { expectedRol: ['admin'] } },
  { path: '**', redirectTo: '', pathMatch: 'full' } // ruta erronea
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
