import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListarProductoComponent } from './components/producto/listar/listar-producto.component';
import { EditarProductoComponent } from './components/producto/editar/editar-producto.component';
import { DetalleProductoComponent } from './components/producto/detalle/detalle-producto.component';
import { NuevoProductoComponent } from './components/producto/nuevo/nuevo-producto.component';
import { LoginUsuarioComponent } from './components/usuario/login/login-usuario.component';
import { MenuComponent } from './components/menu/menu/menu.component';
import { IndexComponent } from './components/index/index/index.component';
import { RegistroComponent } from './components/usuario/registro/registro.component';
import { interceptorProvider } from './services/interceptor.service';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

// externs
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { BorrarComponent } from './components/producto/borrar/borrar.component';


@NgModule({
  declarations: [
    AppComponent,
    ListarProductoComponent,
    EditarProductoComponent,
    DetalleProductoComponent,
    NuevoProductoComponent,
    LoginUsuarioComponent,
    MenuComponent,
    IndexComponent,
    RegistroComponent,
    BorrarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  providers: [interceptorProvider],
  bootstrap: [AppComponent]
})
export class AppModule { }
