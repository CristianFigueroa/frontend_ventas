export class Usuario {
    id?: number; // :? quiere decir que no es un dato obligatorio
    nombre: string;
    nombreUsuario: string;
    email: string;
    password: string;
    rol: string[];

    constructor(nombre: string, nombreUsuario: string, email: string, password: string) {
        this.nombre = nombre;
        this.nombreUsuario = nombreUsuario;
        this.email = email;
        this.password = password;
    }
}
