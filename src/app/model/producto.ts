export class Producto {
  id?: number; // :? quiere decir que no es un dato obligatorio
  nombre: string;
  precio: number;

  constructor(nombre: string, precio: number) {
    this.nombre = nombre;
    this.precio = precio;
  }
}
