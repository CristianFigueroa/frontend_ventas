import { Component, OnInit } from '@angular/core';
import { TokenService } from 'src/app/services/token.service';
import { LoginUsuarioService } from 'src/app/services/login-usuario.service';
import { Router } from '@angular/router';
import { Usuario } from 'src/app/model/usuario';
import { Toast, ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.scss']
})
export class RegistroComponent implements OnInit {

  isLogged: boolean;
  usuario: Usuario;
  nombre: string;
  username: string;
  password: string;
  email: string;
  password2: string;
  isRegisterFail: boolean;


  constructor(
    private tokenService: TokenService,
    private loginService: LoginUsuarioService,
    private router: Router,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    if (this.tokenService.getToken) {
      this.isLogged = false;
      this.isRegisterFail = false;
    }
  }

  registro() {
    this.usuario = new Usuario(this.nombre, this.username, this.email, this.password)
    this.loginService.nuevo(this.usuario).subscribe(
      data =>{
        this.isLogged = true;
        this.isRegisterFail=false;
        this.tokenService.setUsername(data.nombreUsuario);
        this.tokenService.setToken(data.token);
        this.tokenService.setAuthorities(data.authorities);
        this.router.navigate(['/index'])
      },
      error =>{
        this.toastr.error(error.error.mensaje, "Error", {
          timeOut: 3000
        });
        this.isRegisterFail = true;
      }
    )

  
  
  }

}
