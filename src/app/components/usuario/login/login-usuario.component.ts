import { Component, OnInit } from '@angular/core';
import { LoginUsuario } from 'src/app/model/login-usuario';
import { TokenService } from 'src/app/services/token.service';
import { LoginUsuarioService } from 'src/app/services/login-usuario.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-usuario',
  templateUrl: './login-usuario.component.html',
  styleUrls: ['./login-usuario.component.scss']
})
export class LoginUsuarioComponent implements OnInit {

  isLogged: boolean;
  isLoginFail: boolean = false;
  loginUsuario: LoginUsuario;
  nombreUsuario: string;
  password: string;
  roles: string[] = [];

  constructor(
    private tokenService: TokenService,
    private loginService: LoginUsuarioService,
    private router: Router
  ) { }

  ngOnInit(): void {
    // si existe token es porque estamos logeados
    if (this.tokenService.getToken) {
      this.isLogged = false;
      this.isLoginFail = false;
      this.roles = this.tokenService.getAuthorities();
    }
  }

  loggin():void{
    this.loginUsuario = new LoginUsuario(this.nombreUsuario,this.password);
    this.loginService.login(this.loginUsuario).subscribe(
      data => {
        this.isLogged = true;
        this.isLoginFail = false;

        this.tokenService.setUsername(data.nombreUsuario);
        this.tokenService.setToken(data.token);
        this.tokenService.setAuthorities(data.authorities);
        this.roles = data.authorities;
        this.router.navigate(['/index'])
      },
      error =>{
        this.isLogged = false;
        this.isLoginFail = true;
        console.log(error.error.mensaje)
      }
    )
  }

}
