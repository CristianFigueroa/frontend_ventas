import { Component, OnInit } from '@angular/core';
import { Producto } from '../../../model/producto';
import { ProductoService } from '../../../services/producto.service';
import { ToastrService } from 'ngx-toastr';
import { TokenService } from 'src/app/services/token.service';

@Component({
  selector: 'app-listar-producto',
  templateUrl: './listar-producto.component.html',
  styleUrls: ['./listar-producto.component.scss']
})
export class ListarProductoComponent implements OnInit {

  productos: Producto[] = [];
  roles: string[];
  isAdmin = false;

  constructor(
    private productoService: ProductoService,
    private toastr: ToastrService,
    private tokenService: TokenService
  ) { }



  ngOnInit(): void {
    this.cargarProductos();
    this.roles = this.tokenService.getAuthorities();
    this.roles.forEach(rol =>{
      if(rol === "ROLE_ADMIN"){
        this.isAdmin = true;
      }
    })
  }

  cargarProductos(): void {
    this.productoService.list().subscribe(
      data => {
        this.productos = data;
      },
      error => {
        console.log(error);
      }
    );
  }

  borrar(id: number) {
    const confirmacion = confirm('Estas seguro?');
    if (confirmacion && this.isAdmin && this.tokenService.getToken()) {
      this.productoService.delete(id).subscribe(
        data => {
          this.toastr.success('Producto eliminado', 'Ok', {
            timeOut: 3000
          })
          this.cargarProductos();
        },
        error => {
          this.toastr.error(error.error.mensaje, "Error", {
            timeOut: 3000
          });
        }
      )
    }

  }


}
