import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ProductoService } from 'src/app/services/producto.service';
import { ToastrService } from 'ngx-toastr';
import { Producto } from 'src/app/model/producto';

@Component({
  selector: 'app-borrar',
  templateUrl: './borrar.component.html',
  styleUrls: ['./borrar.component.scss']
})
export class BorrarComponent implements OnInit {
  productos: Producto[] = [];

  @Input()
  id: number;

  constructor(
    private productoService: ProductoService,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
  }

  // emite al componente de listar que se elimino un producto, esto con el fin de que se actualice
  @Output() 
  messageEvent = new EventEmitter<string>();

  borrar(id: number) {
    const confirmacion = confirm('Estas seguro?');
    if (confirmacion) {
      this.productoService.delete(id).subscribe(
        data => {
          this.toastr.success('Producto eliminado', 'Ok', {
            timeOut: 3000
          })
          this.messageEvent.emit();
        },
        error => {
          this.toastr.error(error.error.mensaje, "Error", {
            timeOut: 3000
          });
        }
      )
    }

  }
}
