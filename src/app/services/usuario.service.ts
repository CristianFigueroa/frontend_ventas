import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Usuario } from 'src/app/model/usuario';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  usuarioUrl: 'http://localhost:8080/usuario/';

  constructor(private httpClient: HttpClient) { }

  public list(): Observable<Usuario[]> {
    return this.httpClient.get<Usuario[]>(this.usuarioUrl + 'lista');
  }

  public getById(id: number): Observable<Usuario> {
    return this.httpClient.get<Usuario>(this.usuarioUrl + `detalle/${id}`);
  }

  public update(id: number, usuario: Usuario): Observable<any> {
    return this.httpClient.put<any>(this.usuarioUrl + `editar/${id}`, usuario);
  }

  public delete(id: number): Observable<any> {
    return this.httpClient.delete<any>(this.usuarioUrl + `eliminar/${id}`);
  }
}
